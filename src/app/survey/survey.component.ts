import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormArray, FormControl } from '@angular/forms';
import { SurveyService } from './survey.service';
import { SurveyQuestion } from './surveyQuestion.model';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss'],
  animations: [
    trigger('showForm', [
      state('open', style({
        opacity: 1,
      })),
      state('closed', style({
        opacity: 0,
      })),
      transition('open => closed', [
        animate('0.5s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ]),
    trigger('showResult', [
      state('show', style({
        opacity: 1,
      })),
      state('hide', style({
        opacity: 0,
        height: 0,
      })),
      transition('show  => hide', [
        animate('0.5s')
      ]),
      transition('hide => show', [
        animate('0.5s')
      ]),
    ]),
  ]
})

export class SurveyComponent implements OnInit {

  surveyQuestions: SurveyQuestion[];
  surveyForm: FormGroup;
  showForm: boolean = false;
  loaded: boolean = false;
  submitted: boolean = false;

  userAvgScore: number;
  totalAvgScore: number;

  constructor(
    private formBuilder: FormBuilder,
    private surveyService: SurveyService,
  ) { }

  ngOnInit(): void {

    this.surveyForm = this.formBuilder.group({
      questions: this.formBuilder.array([]),
    })

    this.surveyService.getServey().subscribe(r => {
      this.surveyQuestions = r;

      r.forEach(q => {
        (this.surveyForm.get('questions') as FormArray).push(new FormGroup({
          questionKey: new FormControl('', [Validators.required]),
          answer: new FormControl('', [Validators.required])
        }))
      });

      this.loaded = true;
      this.showForm = true;
    });
  }

  get questionGroups() {
    return this.surveyForm.get('questions') as FormArray;
  }

  submitSurvey() {
    if (this.questionGroups.invalid) {
      return;
    }

    this.showForm = false;
    this.loaded = false;
    this.submitted = true;
    this.surveyService.submitSurvey(this.questionGroups.value).subscribe(r => {
      this.userAvgScore = r.userAvg;
      this.totalAvgScore = r.benchAvg;
      this.loaded = true;
    });
  }
}
