import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SurveyService } from './survey.service';
import { SurveyComponent } from './survey.component';
import { MaterialModule } from '../layout/material.module';

@NgModule({
  declarations: [SurveyComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SurveyService]
})
export class SurveyModule { }
