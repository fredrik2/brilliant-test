import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { SurveyQuestion } from './surveyQuestion.model';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  constructor(private http: HttpClient) { }

  public getServey(): Observable<SurveyQuestion[]> {
    return this.http.get(`${environment.backendUrl}/survey`).pipe(take(1)) as any;
  }

  public submitSurvey(answers: any): Observable<any> {
    return this.http.post(`${environment.backendUrl}/survey`, answers).pipe(take(1)) as any;
  }
}
