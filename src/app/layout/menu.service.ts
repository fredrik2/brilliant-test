import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private _menu: MenuItem[] = [
    {
      route: '/survey',
      caption: 'Take survey',
    },

  ]

  constructor() {}

  public menu(): MenuItem[] {
    return this._menu;
  }
}

class MenuItem {
  route: string;
  caption: string;
}
